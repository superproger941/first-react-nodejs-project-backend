import {Entity, Column, PrimaryGeneratedColumn} from 'typeorm';

@Entity()
export class Transaction {
    @PrimaryGeneratedColumn()
    id: number;

    //unixtime — время создания
    @Column({nullable: true, type: "timestamp"})
    created_dt: string;

    //пользователь
    @Column()
    user_id: number;

    //номер транзакции
    @Column({nullable: true, type: "float"})
    tx_id: number;

    //сумма, если >0 то приход, если <0 то расход
    @Column({nullable: true, type: "float"})
    tx_amt: number;

    //баланс на конец
    @Column({nullable: true, type: "float"})
    total_amt: number;
}