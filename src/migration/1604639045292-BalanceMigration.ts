import {MigrationInterface, QueryRunner} from "typeorm";

export class BalanceMigration1604639045292 implements MigrationInterface {
    name = 'BalanceMigration1604639045292'

    public async up(queryRunner: QueryRunner): Promise<void> {
        await queryRunner.query(`CREATE TABLE "balance" ("id" SERIAL NOT NULL, "user_id" integer NOT NULL, "investment_id" integer NOT NULL, "incoming_reward" integer NOT NULL, "incoming_tx" integer NOT NULL, "balance_amt" integer NOT NULL, "cycle_in" integer NOT NULL, CONSTRAINT "PK_079dddd31a81672e8143a649ca0" PRIMARY KEY ("id"))`);
        await queryRunner.query(`ALTER TABLE "transaction" DROP COLUMN "balance_amt"`);
    }

    public async down(queryRunner: QueryRunner): Promise<void> {
        await queryRunner.query(`ALTER TABLE "transaction" ADD "balance_amt" integer`);
        await queryRunner.query(`DROP TABLE "balance"`);
    }

}
