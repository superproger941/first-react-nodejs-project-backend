import {MigrationInterface, QueryRunner} from "typeorm";

export class DatesMigration1604873881110 implements MigrationInterface {

    public async up(queryRunner: QueryRunner): Promise<void> {
        await queryRunner.query(`ALTER TABLE "transaction" DROP COLUMN "created_dt"`);
        await queryRunner.query(`ALTER TABLE "investment" DROP COLUMN "created_dt"`);
        await queryRunner.query(`ALTER TABLE "transaction" ADD "created_dt" timestamp`);
        await queryRunner.query(`ALTER TABLE "investment" ADD "created_dt" timestamp`);
    }

    public async down(queryRunner: QueryRunner): Promise<void> {
        await queryRunner.query(`ALTER TABLE "investment" DROP COLUMN "created_dt"`);
        await queryRunner.query(`ALTER TABLE "investment" ADD "created_dt" character varying NOT NULL`);
        await queryRunner.query(`ALTER TABLE "transaction" DROP COLUMN "created_dt"`);
        await queryRunner.query(`ALTER TABLE "transaction" ADD "created_dt" character varying NOT NULL`);
    }

}
