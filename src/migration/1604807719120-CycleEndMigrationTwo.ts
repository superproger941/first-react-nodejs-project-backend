import {MigrationInterface, QueryRunner} from "typeorm";

export class CycleEndMigrationTwo1604807719120 implements MigrationInterface {
    name = 'CycleEndMigrationTwo1604807719120'

    public async up(queryRunner: QueryRunner): Promise<void> {
        await queryRunner.query(`ALTER TABLE "balance" ADD "cycle_out" integer`);
        await queryRunner.query(`ALTER TABLE "balance" ADD "total" integer`);
    }

    public async down(queryRunner: QueryRunner): Promise<void> {
        await queryRunner.query(`ALTER TABLE "balance" DROP COLUMN "total"`);
        await queryRunner.query(`ALTER TABLE "balance" DROP COLUMN "cycle_out"`);
    }

}
