import {MigrationInterface, QueryRunner} from "typeorm";

export class FloatMigration1604810904233 implements MigrationInterface {
    name = 'FloatMigration1604810904233'

    public async up(queryRunner: QueryRunner): Promise<void> {
        await queryRunner.query(`ALTER TABLE "balance" DROP COLUMN "incoming_reward"`);
        await queryRunner.query(`ALTER TABLE "balance" ADD "incoming_reward" double precision`);
        await queryRunner.query(`ALTER TABLE "balance" DROP COLUMN "incoming_tx"`);
        await queryRunner.query(`ALTER TABLE "balance" ADD "incoming_tx" double precision`);
        await queryRunner.query(`ALTER TABLE "balance" DROP COLUMN "balance_amt"`);
        await queryRunner.query(`ALTER TABLE "balance" ADD "balance_amt" double precision`);
        await queryRunner.query(`ALTER TABLE "balance" DROP COLUMN "cycle_in"`);
        await queryRunner.query(`ALTER TABLE "balance" ADD "cycle_in" double precision`);
        await queryRunner.query(`ALTER TABLE "balance" DROP COLUMN "reward_id"`);
        await queryRunner.query(`ALTER TABLE "balance" ADD "reward_id" double precision`);
        await queryRunner.query(`ALTER TABLE "balance" DROP COLUMN "reward_amt"`);
        await queryRunner.query(`ALTER TABLE "balance" ADD "reward_amt" double precision`);
        await queryRunner.query(`ALTER TABLE "investment" DROP COLUMN "tx_amt"`);
        await queryRunner.query(`ALTER TABLE "investment" ADD "tx_amt" double precision`);
        await queryRunner.query(`ALTER TABLE "investment" DROP COLUMN "net_amt"`);
        await queryRunner.query(`ALTER TABLE "investment" ADD "net_amt" double precision`);
        await queryRunner.query(`ALTER TABLE "investment" DROP COLUMN "tx_returned_amt"`);
        await queryRunner.query(`ALTER TABLE "investment" ADD "tx_returned_amt" double precision`);
        await queryRunner.query(`ALTER TABLE "transaction" DROP COLUMN "tx_id"`);
        await queryRunner.query(`ALTER TABLE "transaction" ADD "tx_id" double precision`);
        await queryRunner.query(`ALTER TABLE "transaction" DROP COLUMN "tx_amt"`);
        await queryRunner.query(`ALTER TABLE "transaction" ADD "tx_amt" double precision`);
        await queryRunner.query(`ALTER TABLE "transaction" DROP COLUMN "total_amt"`);
        await queryRunner.query(`ALTER TABLE "transaction" ADD "total_amt" double precision`);
    }

    public async down(queryRunner: QueryRunner): Promise<void> {
        await queryRunner.query(`ALTER TABLE "transaction" DROP COLUMN "total_amt"`);
        await queryRunner.query(`ALTER TABLE "transaction" ADD "total_amt" integer`);
        await queryRunner.query(`ALTER TABLE "transaction" DROP COLUMN "tx_amt"`);
        await queryRunner.query(`ALTER TABLE "transaction" ADD "tx_amt" integer`);
        await queryRunner.query(`ALTER TABLE "transaction" DROP COLUMN "tx_id"`);
        await queryRunner.query(`ALTER TABLE "transaction" ADD "tx_id" integer`);
        await queryRunner.query(`ALTER TABLE "investment" DROP COLUMN "tx_returned_amt"`);
        await queryRunner.query(`ALTER TABLE "investment" ADD "tx_returned_amt" integer`);
        await queryRunner.query(`ALTER TABLE "investment" DROP COLUMN "net_amt"`);
        await queryRunner.query(`ALTER TABLE "investment" ADD "net_amt" integer`);
        await queryRunner.query(`ALTER TABLE "investment" DROP COLUMN "tx_amt"`);
        await queryRunner.query(`ALTER TABLE "investment" ADD "tx_amt" integer`);
        await queryRunner.query(`ALTER TABLE "balance" DROP COLUMN "reward_amt"`);
        await queryRunner.query(`ALTER TABLE "balance" ADD "reward_amt" integer`);
        await queryRunner.query(`ALTER TABLE "balance" DROP COLUMN "reward_id"`);
        await queryRunner.query(`ALTER TABLE "balance" ADD "reward_id" integer`);
        await queryRunner.query(`ALTER TABLE "balance" DROP COLUMN "cycle_in"`);
        await queryRunner.query(`ALTER TABLE "balance" ADD "cycle_in" integer`);
        await queryRunner.query(`ALTER TABLE "balance" DROP COLUMN "balance_amt"`);
        await queryRunner.query(`ALTER TABLE "balance" ADD "balance_amt" integer`);
        await queryRunner.query(`ALTER TABLE "balance" DROP COLUMN "incoming_tx"`);
        await queryRunner.query(`ALTER TABLE "balance" ADD "incoming_tx" integer`);
        await queryRunner.query(`ALTER TABLE "balance" DROP COLUMN "incoming_reward"`);
        await queryRunner.query(`ALTER TABLE "balance" ADD "incoming_reward" integer`);
    }

}
