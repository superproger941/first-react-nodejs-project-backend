import {MigrationInterface, QueryRunner} from "typeorm";

export class BalanceMigrationNew1604698937031 implements MigrationInterface {
    name = 'BalanceMigrationNew1604698937031'

    public async up(queryRunner: QueryRunner): Promise<void> {
        await queryRunner.query(`ALTER TABLE "transaction" DROP COLUMN "reward_amt"`);
        await queryRunner.query(`ALTER TABLE "balance" ADD "reward_amt" integer NOT NULL`);
        await queryRunner.query(`ALTER TABLE "transaction" DROP COLUMN "reward_id"`);
        await queryRunner.query(`ALTER TABLE "balance" ADD "reward_id" integer NOT NULL`);
    }

    public async down(queryRunner: QueryRunner): Promise<void> {
        await queryRunner.query(`ALTER TABLE "balance" DROP COLUMN "reward_amt"`);
        await queryRunner.query(`ALTER TABLE "transaction" ADD "reward_amt" integer`);
        await queryRunner.query(`ALTER TABLE "balance" DROP COLUMN "reward_id"`);
        await queryRunner.query(`ALTER TABLE "transaction" ADD "reward_id" integer`);
    }

}
