import {Module} from '@nestjs/common';

import {JwtModule} from '@nestjs/jwt';
import {TypeOrmModule} from "@nestjs/typeorm";

import {UsersModule} from '../users/users.module';
import {PassportModule} from '@nestjs/passport';
import {JwtStrategy} from './jwt.strategy';
import {AuthService} from './auth.service';

import {User} from "../entitites/user.entity";
import {AuthController} from "./auth.controller";

import {jwtConstants} from './constants';
import {InvestmentRepository} from "../investment/investment.repository";
import {BalanceRepository} from "../balance/balance.repository";

@Module({
    imports: [
        TypeOrmModule.forFeature([
            User,
            InvestmentRepository,
            BalanceRepository
        ]),
        UsersModule,
        PassportModule.register({defaultStrategy: 'jwt'}),
        JwtModule.register({
            secret: jwtConstants.secret,
            signOptions: {expiresIn: '86400s'},
        }),
    ],
    controllers: [AuthController],
    providers: [AuthService, JwtStrategy],
})
export class AuthModule {
}
