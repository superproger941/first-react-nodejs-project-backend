import {Controller, Request, Post, UseGuards, Get, Body, Logger} from '@nestjs/common';
import {AuthService} from './auth.service';

import {JwtAuthGuard} from './jwt-auth.guard';
import {RegistrationDto} from "./dto/registration.dto";
import {LoginDto} from "./dto/login.dto";

@Controller('auth')
export class AuthController {
    constructor(private readonly authService: AuthService) {
    }

    @Post('registration')
    registration(@Body() registrationDto: RegistrationDto) {
        return this.authService.registration(registrationDto);
    }

    //@UseGuards(LocalAuthGuard)
    @Post('login')
    async login(@Body() loginDto: LoginDto) {
        return this.authService.login(loginDto);
    }

    @Get('transactions')
    @UseGuards(JwtAuthGuard)
    transactions(@Request() request) {
        return this.authService.transactions(request.user.userId);
    }

    @Get('investments')
    @UseGuards(JwtAuthGuard)
    investments(@Request() request) {
        return this.authService.investments();
    }

    @UseGuards(JwtAuthGuard)
    @Get('profile')
    getProfile(@Request() req) {
        return req.user;
    }

    @Post('logout')
    logout(): string {
        return null;
    }

    @Get('begin')
    //@UseGuards(JwtAuthGuard)
    begin(@Request() request) {
        return this.authService.begin();
    }

    @Get('end')
    //UseGuards(JwtAuthGuard)
    end(@Request() request) {
        return this.authService.end();
    }
}
