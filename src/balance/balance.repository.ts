import {Balance} from '../entitites/balance.entity';
import {EntityRepository, getConnection, Repository} from 'typeorm';
import {TransactionRepository} from "../transaction/transaction.repository";
import {User} from "../entitites/user.entity";

@EntityRepository(Balance)
export class BalanceRepository extends Repository<Balance> {
    saveBalance = async (balance: Balance) => {
        return await this.save(balance);
    };

    beginCycleUpdateBalances = async (investment, prevInvestment, beforeLastInvestment) => {
        const users = await getConnection()
            .getRepository(User)
            .createQueryBuilder("user")
            .getMany();
        const balanceRepository = await getConnection().getCustomRepository(BalanceRepository);
        for (const user of users) {
            const userId = user.id;

            let balance = await balanceRepository
                .createQueryBuilder('balance')
                .where("balance.user_id = :user_id", {user_id: userId})
                .andWhere("balance.investment_id = :investment_id", {investment_id: investment.id})
                .getOne();

            if (!balance) {
                balance = new Balance();
            }

            balance.user_id = userId;
            balance.investment_id = investment.id;

            const beforeLastBalance = beforeLastInvestment ? await this.getBalanceByInvestmentIdAndUserId(
                beforeLastInvestment.id,
                userId
            ) : 0;
            balance.incoming_reward = beforeLastBalance ? beforeLastBalance.reward_amt : 0;

            const transactionRepository = await getConnection().getCustomRepository(TransactionRepository);
            const transactions = await transactionRepository
                .getTransactionsByUserIdAndBetweenTimeInterval(
                    userId, prevInvestment ? prevInvestment.created_dt : investment.created_dt, investment.created_dt
                );
            console.log('transactions ', transactions);
            balance.incoming_tx = 0;
            transactions.forEach((item) => {
                balance.incoming_tx += item.tx_amt;
            });

            const lastBalance = prevInvestment ? await this.getBalanceByInvestmentIdAndUserId(
                prevInvestment.id,
                userId
            ) : 0;

            const lastBalanceAmt = lastBalance ? lastBalance.balance_amt : 0;
            const beforeLastBalanceAmt = beforeLastBalance ? beforeLastBalance.reward_amt : 0;
            console.log('balance.incoming_tx ', balance.incoming_tx);
            console.log('lastBalanceAmt ', lastBalanceAmt);
            console.log('beforeLastBalanceAmt ', beforeLastBalanceAmt);
            balance.balance_amt = balance.incoming_tx + lastBalanceAmt + beforeLastBalanceAmt;

            const balances = await this.getBalancesByInvestmentId(investment.id);

            let allUsersBalances = 0;
            balances.forEach((item) => {
                allUsersBalances += item.balance_amt;
            });
            balance.cycle_in = Math.floor(balance.balance_amt ? balance.balance_amt / ((allUsersBalances + balance.balance_amt) * investment.tx_amt) : 0);

            balance.reward_id = 0;
            balance.reward_amt = 0;
            balance.cycle_out = 0;
            balance.total = 0;

            console.log(await balanceRepository.saveBalance(balance));
        }
    };

    getBalanceByInvestmentIdAndUserId = async (investmentId, userId) => {
        return await getConnection()
            .getRepository(Balance)
            .createQueryBuilder("balance")
            .where(
                "balance.investment_id = :investment_id", {investment_id: investmentId}
            )
            .andWhere("balance.user_id = :user_id", {user_id: userId})
            .getOne();
    }

    getBalancesByInvestmentId = async (investmentId) => {
        return await getConnection()
            .getRepository(Balance)
            .createQueryBuilder("balance")
            .where(
                "balance.investment_id = :investment_id", {investment_id: investmentId}
            )
            .getMany();
    }

    endCycleUpdateBalances = async (investment) => {
        const users = await getConnection()
            .getRepository(User)
            .createQueryBuilder("user")
            .getMany();
        const balanceRepository = await getConnection()
            .getCustomRepository(BalanceRepository);
        const rewardInvestment = investment.tx_returned_amt - investment.tx_amt;
        for (const user of users) {
            const userId = user.id;

            let balance = await balanceRepository
                .createQueryBuilder('balance')
                .where("balance.user_id = :user_id", {user_id: userId})
                .andWhere("balance.investment_id = :investment_id", {investment_id: investment.id})
                .getOne();

            if (!balance) {
                return;
            }
            let cycleInSums = 0;
            for (const user of users) {
                let balanceAgain = await this.getBalanceByInvestmentIdAndUserId(
                    investment.id,
                    userId
                );
                cycleInSums += balanceAgain.cycle_in;
            }
            balance.reward_id = 0;
            balance.reward_amt = (rewardInvestment * balance.cycle_in) / cycleInSums;
            balance.cycle_out = balance.cycle_in + balance.reward_amt;
            balance.total = balance.reward_amt + balance.balance_amt;

            console.log(await balanceRepository.saveBalance(balance));
        }
    }
}