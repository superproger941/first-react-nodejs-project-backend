import {DynamicModule, Module} from '@nestjs/common';
import {AppController} from './app.controller';
import {AppService} from './app.service';
import {AuthModule} from "./auth/auth.module";
import {User} from "./entitites/user.entity";
import {TypeOrmModule} from '@nestjs/typeorm';
import {Investment} from "./entitites/investment.entity";
import {Transaction} from "./entitites/transaction.entity";
import {ConfigModule} from 'nestjs-config';
import * as path from 'path';
import {Balance} from "./entitites/balance.entity";

@Module({
    imports: [
        TypeOrmModule.forRoot({
            type: 'postgres',
            host: 'localhost',
            port: 5432,
            username: 'postgres',
            password: '123',
            database: 'postgres',
            entities: [User, Transaction, Investment, Balance],

            // We are using migrations, synchronize should be set to false.
            synchronize: true,

            // Run migrations automatically,
            // you can disable this if you prefer running migration manually.
            migrationsRun: true,
            logging: true,
            logger: 'file',

            // allow both start:prod and start:dev to use migrations
            // __dirname is either dist or src folder, meaning either
            // the compiled js in prod or the ts in dev
            migrations: [__dirname + '/migrations/**/*{.ts,.js}'],
            cli: {
                migrationsDir: 'src/migrations',
            },
        }),
        AuthModule,
        ConfigModule.load(path.resolve(__dirname, 'config', '**/!(*.d).{ts,js}')),
    ],
    controllers: [AppController],
    providers: [AppService],
})

export class AppModule {
}


