import {EntityRepository, getConnection, Repository} from 'typeorm';
import {Transaction} from "../entitites/transaction.entity";

@EntityRepository(Transaction)
export class TransactionRepository extends Repository<Transaction> {
    getTransactionsByUserIdAndBetweenTimeInterval = async (userId, begin, end) => {
        return await getConnection()
            .getRepository(Transaction)
            .createQueryBuilder("transaction")
            .where(
                "transaction.user_id = :user_id", {user_id: userId}
            )
            .andWhere(
                "transaction.created_dt >= :begin AND transaction.created_dt < :end",
                {begin, end}
            )
            .getMany();
    }
}